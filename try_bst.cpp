#include<iostream>
#include "BST.hpp"

int main(){
    BST<int,int> att1;
    att1[5];
    att1[6];
    att1[4];
    att1[5];
    att1[7];
    while(true){
        int input;
        std::cout << "Enter a number to check (enter 0 to close): ";
        std::cin >> input;
        if(input == 0 ){
            break;
        }else if(att1.contains(input)){
            std::cout << "it contains " << input << "\n";
        }else{
            std::cout << "it does not contain " << input << "\n";
        }
    }
}