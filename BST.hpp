#ifndef _BST_HPP_
#define _BST_HPP_

#include <vector>

template <typename K, typename V>
struct Node {
  K key;
  V value;
  Node<K,V>* left;
  Node<K,V>* right;
  
  Node(K key, V value) {
    this->key = key;
    this->value = value;
    left = right = nullptr;
  }

  Node(const Node<K,V>& other) {
    key = other.key;
    value = other.value;
    left = other.left;
    right = other.right;
  }
};


template <typename K, typename V>
class BST {
private:
  Node<K,V>* root;

  V& get(Node<K,V>* root, K key) {
    if (key == root->key) {
      return root->value;
    } else if (key < root->key) {
      if (!root->left){
	root->left = new Node<K,V>(key, V());
	return root->left->value;
      }
      return get(root->left, key);
    } else {
      if (!root->right){
	root->right = new Node<K,V>(key, V());
	return root->right->value;
      }
      return get(root->right, key);
    }
  }

  Node<K,V>* min(Node<K,V>* node) {
    if (!node->left)
      return node;
    else
      return min(node->left);
  }

  bool contains(Node<K,V>* node, K key) {
    if (!node)
      return false;

    if (node->key == key)
      return true;
    else if (key < node->key)
      return contains(node->left, key);
    else
      return contains(node->right, key);
  }

  Node<K,V>* remove (Node<K,V>* node, K key) {
    if (!node)
      return nullptr;
    
    if (key < node->key) {
      // smaller: remove from left
      node->left = remove(node->left, key);
    } else if (key > node->key) {
      // larger: remove from right
      node->right = remove(node->right, key);
    } else {
      // found node to remove
      if (!node->right) { // 1 link, replace with left
	Node<K,V>* temp = node->left;
	delete node;
	return temp;
      }
      
      if (!node->left) { // 1 link, replace with right
	Node<K,V>* temp = node->right;
	delete node;
	return temp;
      }

      // 2 links: swap node with min from the right
      Node<K,V>* temp = node;

      // copy min(temp->right) as it will be removed below
      node = new Node<K,V>(*min(temp->right));
	
      node->right = remove_min(temp->right);
      node->left = temp->left;
      delete temp;
    }
    return node;
  }
  
  Node<K,V>* remove_min(Node<K,V>* node) {
    if (!node->left) {
      Node<K,V>* temp = node->right;
      delete node;
      return temp;
    }
    node->left = remove_min(node->left);
    return node;
  }
  
  void addkeys(Node<K,V>* node, std::vector<K>& thekeys) {
    if (node != nullptr) {
      // add keys in order
      addkeys(node->left, thekeys);
      thekeys.push_back(node->key);
      addkeys(node->right, thekeys);
    }
  }  

  
public:

  BST() {
    root = nullptr;
  }

  ~BST() {
    while (root)
      remove(root->key);
  }

  V& operator[](K key) {
    if (!root) {
      root = new Node<K,V>(key, V());
      return root->value;
    }
    
    return get(root, key);
  }

  bool contains(K key) {
    return contains(root, key);
  }

  void remove(K key) {
    root = remove(root, key);
  }

  std::vector<K> keys() {
    std::vector<K> thekeys;
    addkeys(root, thekeys);
    return thekeys;
  }

};

#endif


